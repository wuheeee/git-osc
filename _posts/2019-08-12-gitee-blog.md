---
layout: post
title: "码云 Gitee 率先支持中国开源许可证 —— 木兰宽松许可证"
---

<p>为响应开源产业发展需求，本着立足本土、面向全球、开放透明的原则，北京大学作为国家重点研发计划&ldquo;云计算和大数据开源社区生态系统&rdquo;的子任务牵头单位，依托全国信标委云计算标准工作组和中国开源云联盟，联合国内开源生态圈产学研各界优势团队、开源社区以及拥有丰富知识产权相关经验的众多律师，共同研制形成重要成 果&mdash;&mdash;木兰系列开源许可证的《木兰宽松许可证，第 1 版》，于 2019年8月5日在中国开源云联盟官网正式上线 (<a href="http://license.coscl.org.cn/MulanPSL/">http://license.coscl.org.cn/MulanPSL/</a>)。</p>

<p>木兰宽松许可证，具有以下特点：</p>

<ol>
	<li>许可证内容以中英文双语表述，中英文版本具有同等法律效力，方便更多的开源参与者阅读使用，简化了中国使用者进行法律解释时的复杂度。</li>
	<li>许可证明确授予用户永久性、全球性、免费的、非独占的、不可撤销的版权和专利许可，并针对目前专利联盟存在的互诉漏洞问题，明确规定禁止&ldquo;贡献者&rdquo;或&ldquo;关联实体&rdquo;直接或间接地（通过代理、专利被许可人或受让人）进行专利诉讼或其他维权行动，否则终止专利授权。</li>
	<li>许可证明确不提供对&ldquo;贡献者&rdquo;的商品名称、商标、服务标志等的商标许可，保护&ldquo;贡献者&rdquo;的切身利益。</li>
	<li>许可证经技术专家和法律专家共同修订，在明确合同双方行为约束的前提下尽可能地精简条款、优化表述，降低产生法律纠纷的风险。</li>
</ol>

<p>开源中国作为国家重点研发计划&ldquo;云计算和大数据开源社区生态系统&rdquo;的参与单位，旗下码云代码托管平台率先增加对木兰宽松许可证的支持。开发者可在创建仓库时候直接选择木兰许可证。已有开源项目需要将许可证更改为木兰许可证的，可通过 gitee.com 网站删除 LICENSE 文件，在选择新建 LICENSE 文件，选择木兰许可证即可，如下图所示：</p>

<p>新建仓库选择木兰许可证：</p>

<p><img src="https://static.oschina.net/uploads/space/2019/0812/222609_1NK0_12.png" width="600" /></p>

<p>如果是已有开源项目要添加木兰许可证或者是修改为木兰许可证，可先删除 LICENSE 文件，然后新建 LICENSE 文件选择木兰许可证：</p>

<p><img src="https://oscimg.oschina.net/oscnet/6ab95f308dee57064c56e8d3543aa05ae5d.jpg" width="600" /></p>

<p><img src="https://static.oschina.net/uploads/space/2019/0812/223601_Dphk_12.png" width="600" /></p>

<p>希望越来越多的开源项目选择木兰许可证。</p>

<p>码云 &mdash;&mdash; 全力支持国内开源生态的发展，目前已经 600 万仓库，200 万的开源仓库。欢迎&ldquo;放码过来&rdquo;！你可通过&nbsp;<a href="https://gitee.com/explore">https://gitee.com/explore</a> 开源页面来访问编辑推荐的开源项目。</p>

<p>更多关于木兰许可证信息请阅读&nbsp;<a href="http://license.coscl.org.cn/MulanPSL/">http://license.coscl.org.cn/MulanPSL/</a></p>
