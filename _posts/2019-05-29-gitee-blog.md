---
layout: post
title: "SVN 的文件和目录只读特性，能否在 Git 也实现？"
---

<p><strong>码云六周年系列更新第二弹&nbsp;&mdash;&mdash; Git 只读文件支持！！！</strong></p>

<p><img alt="" height="109" src="https://oscimg.oschina.net/oscnet/54722d453b8ec4878c11b4642bdb602ac3f.jpg" width="350" /></p>

<p>作为一个应用项目，一般会有一些文件来描述生产环境中的配置信息，例如服务连接信息、环境配置信息等。而在本地开发过程中又必须依照本地的服务环境来调整配置这些文件，而如果不小心提交了这些修改，可能会直接导致生成环境中的应用无法正常运行。</p>

<p>所以在 SVN 上有一个非常有用的特性，可以配置某些文件是只读的，不允许提交修改。</p>

<p>而 Git 本身是没有这个特性的，主流的 Git 平台一般会提供只读分支的功能，但无法实现对个别文件或者文件夹的只读限制。</p>

<p>------ 此次有分割线 -------</p>

<p><span style="color:#c0392b"><strong>但是！ 全球独一份，码云现在提供了这个功能！！</strong></span></p>

<p>使用方法如下：</p>

<p>1. 进入仓库页面，右键要设置为只读的文件或者文件夹，选择&ldquo;标识为只读&rdquo;（仓库管理员才有权限）</p>

<p><img src="https://oscimg.oschina.net/oscnet/1dbe11d1cfe4af55e81bbd09299fb65c61d.jpg" width="600" /></p>

<p><img src="https://oscimg.oschina.net/oscnet/9c3d9fc10ae189e70c55a2499b8baa0a344.jpg" width="600" /></p>

<p>2. 进入仓库管理界面，可以查看和管理仓库所有的只读设置</p>

<p><img src="https://oscimg.oschina.net/oscnet/516d97109b515e4f8c19c427d7828c2be22.jpg" width="600" /></p>

<p>3. 如果推送代码时候，包含对只读文件的修改则会报错：</p>

<p>代码提交报错：</p>

<p><img alt="è¾å¥å¾çè¯´æ" src="https://gitee.com/uploads/images/2019/0423/152506_6520e460_4764813.png" width="600" /></p>

<p>PR 代码合并报错：</p>

<p><img alt="è¾å¥å¾çè¯´æ" src="https://gitee.com/uploads/images/2019/0410/111243_31f21bb2_4764813.png" width="600" /></p>

<p><strong>该功能目前面向所有企业版的用户提供，包括免费和付费的企业版以及高校版用户。</strong></p>

<p>前往体验 <a href="https://gitee.com/enterprises?from=readonly_news">https://gitee.com/enterprises</a>&nbsp;</p>
