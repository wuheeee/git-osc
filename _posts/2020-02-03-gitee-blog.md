---
layout: post
title: "今天开始远程办公了，送上远程办公实用指南"
---

<p>高效的远程协作，关键在于成员之间达成工作规则共识、任务流转清晰可追溯、信息及时同步。</p>

<p><a href="https://gitee.com/enterprises">Gitee</a>&nbsp;基于研发团队的通用场景，整理了一个远程工作的约定<strong>范本</strong>，希望对您的团队远程实践有所助益。</p>

<h3>1 工作约定</h3>

<h4>1.1 工作时间</h4>

<ul>
	<li>工作时间为________（例如：9:00-12:00，14:00-18:00），期间请尽可能独处于安静的空间。</li>
	<li>工作时间内应当保持电脑、工作软件、工作沟通的 IM 软件开启。</li>
	<li>中途因私离开时间超过 ______（例如：20 分钟）应当事先报备直接主管 。</li>
</ul>

<h4>1.2 信息同步机制</h4>

<p>建立密切联系和信任，每天定时同步工作安排、进度。</p>

<ul>
	<li><strong>每日站会</strong>改为<strong>群语音</strong>&nbsp;&nbsp;<br />
	小组 leader 汇报阶段目标及进度、当日工作安排 。<br />
	每位成员发言，同步自己的工作目标、进度、当日安排、所需的支持 。</li>
	<li><strong>对于短线工作</strong>（两小时内可以完成的），随时在 IM 沟通进度 。</li>
	<li><strong>对于持续性工作、或耗时较长的一次性工作</strong>，需定时主动同步阶段性产出，例如于每天 11:40、18:00 各同步一次 。</li>
	<li><strong>需要他人响应的工作事项</strong>，需明确响应时间和方式，并提前主动跟进结果。</li>
	<li><strong>下班前</strong>，在 Gitee 企业版的【<strong>周报】中</strong>更新工作日志。</li>
</ul>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%91%A8%E6%8A%A5.png" /></p>

<h3>2 远程开发基本配置</h3>

<p><a href="https://gitee.com/enterprises">Gitee 企业版</a>提供专属、独立的企业空间，管理员可为每位成员分配角色和权限，成员的帐号相互独立，对企业空间、企业资源的访问、操作等严格受企业空间的规则制约。</p>

<p>使用<a href="https://gitee.com/enterprises">&nbsp;Gitee 企业版</a>进行远程开发协作，管理员可提前准备以下工作事项：</p>

<h4>2.1 邀请成员加入企业</h4>

<p>可发送邀请链接到微信群/ QQ 群里，方便地批量邀请同事加入企业版，并根据实际需要分配不同的【成员角色】。通常为【管理员】和【普通成员】。</p>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%9B%BE%E7%89%872.png" /></p>

<h4>2.2 创建项目</h4>

<p><a href="https://gitee.com/enterprises">Gitee 企业版</a>采用【项目】的维度管理研发项目，并且给每个项目配备了独立的【项目视图】进行单独管理&mdash;&mdash;围绕特定目标所需开展一系列过程管理，有效整合相关的资源和数据，并提供一系列可视化工具，使协作更聚焦、透明、高效。</p>

<p>创建一个新项目时，可以指定部分企业成员为这个项目的成员，被指定的项目成员才可以参与此项目。</p>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%9B%BE%E7%89%873.png" /></p>

<h4>2.3 创建代码仓库</h4>

<p>创建项目成功后，进入该项目的【项目视图】，并打开【仓库】页面，创建一个与该项目关联的代码仓库。</p>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%9B%BE%E7%89%874.png" /></p>

<p>仓库成员（可以对仓库进行操作的企业成员）和项目成员的范围并非等同，管理员可以根据实际需要自行配置。</p>

<h3>3 开发工作流程</h3>

<h4>3.1 制定目标和计划</h4>

<p>在远程办公中，我们首先要制定具备可操作性、清晰且高效的目标和计划。</p>

<p>在【项目】中，可使用【任务】中的【里程碑】功能以工作模块/时间等维度对项目工作进行二级拆分，确定各阶段工作目标和各时间周期内的整体计划。【里程碑】可继续拆分为任务、子任务（支持无限级）。</p>

<p>在实际操作中，我们通常通过语音会议讨论目标和计划的具体细节，并根据讨论结果，在&nbsp;<a href="https://gitee.com/enterprises">Gitee 企业版</a>的【文档】中创建相关文档以描述目标和计划，并根据描述在&nbsp;<a href="https://gitee.com/enterprises">Gitee 企业版</a>中创建相应的【项目】、【仓库】、【里程碑】和【任务】。</p>

<p>目标和计划创建成功，相关成员会收到即时通知。建议通过 IM 工具进一步通知同事并要求反馈，以确保周知。</p>

<h4>3.2 分配任务</h4>

<p>举个例子，我们需要在项目中做一个新功能，负责的同事在该项目中创建一个任务，将大致的想法在其中描述，并且把相关的同事添加为此任务的协作者。协作者可以对任务进行评论或者编辑。</p>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%9B%BE%E7%89%875.png" /></p>

<p>该任务经过沟通讨论定稿之后，项目管理人员更改任务状态为【进行中】，根据任务本身拆分的情况建立子任务，并指定相应的子任务负责人。</p>

<p><img alt="实用范本——远程工作的约定-Gitee 官方博客" src="https://blog.gitee.com/wp-content/uploads/2020/02/%E5%9B%BE%E7%89%876.png" /></p>

<p>任务创建完成后，项目管理人员将通过 IM 工具通知到各任务及子任务的负责人，并开始跟踪项目进度。<br />
此流程的适用场景很多，例如产品人员可以用来提出新的需求，测试人员指派开发人员修复 bug，开发人员提醒测试人员对产出进行测试等等。</p>

<h4>3.3 开发协作</h4>

<p>开发人员在本地完成自己的任务后，将代码提交到&nbsp;<a href="https://gitee.com/enterprises">Gitee 企业版</a>内指定【项目】中指定的【仓库】上，并且向由管理员指定的合并分支提出合并请求（Pull Request）。<br />
了解更多关于开发协作的细节，请查看&nbsp;<a href="https://gitee.com/help/articles/4128#article-header0" target="_blank">Fork+Pull 开发模式。</a></p>

<h4>3.4 代码审核与合并</h4>

<p>管理员收到开发人员提交的合并请求，可以对合并请求的内容进行代码审查（Code Review），通过人员审查和持续集成工具测试后，将开发人员的改动合并到要进行发布的分支，最终由专人进行发布。（<a href="https://gitee.com/help/articles/4193#article-header0" target="_blank">点击此处</a>了解更多部署第三方持续集成工具）</p>

<p>》》<strong>免费开通 Gitee 企业版</strong>：<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>

<p><a href="https://gitee.com/enterprises"><img alt="实用范本——远程工作的约定-Gitee 官方博客" height="880" src="https://blog.gitee.com/wp-content/uploads/2020/02/70F2077786B3BEC58984C17EE2BB4A27-1.png__thumbnail-1-923x2000.png" width="406" /></a></p>

<p><strong>免费开通 Gitee 企业版</strong>：<a href="https://gitee.com/enterprises">https://gitee.com/enterprises</a></p>
