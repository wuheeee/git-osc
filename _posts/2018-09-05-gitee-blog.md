---
layout: post
title: "码云已经支持 Git Wire Protocol （Git v2）"
---

<p>两个多月过去了，我们可以大声的告诉大家，码云目前已经支持 Git Wire Protocol。

码云的 Git SSH 服务器并不是 OpenSSH，而是基于 libssh 开发的一个服务，叫 Basalt Sshd。</p>