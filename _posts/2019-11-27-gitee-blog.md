---
layout: post
title: "Gitee 通过 ISO27001 安全认证与 ISO9001 质量认证"
---

<p>最近，Gitee 正式通过了：</p>

<ul>
	<li><strong>IS0/IEC 27001:2013&nbsp;</strong>信息安全管理体系认证；</li>
	<li><strong>ISO9001:2015&nbsp;</strong>质量管理体系认证。</li>
</ul>

<p><img alt="Gitee 通过 ISO27001 与 ISO9001 认证-码云 Gitee 官方博客" height="296" src="https://pic2.zhimg.com/80/v2-1b6837523f82e9f4c961d9101c5da6c1_hd.jpg" width="394" /></p>

<p>ISO27001 是目前国际上最权威、最严格、也是全球应用最广泛与典型的信息安全管理体系标准，涵盖安全策略、资产管理、物理和环境安全、通信和操作管理、访问控制、系统采集开发和维护、信息安全事故管理、业务连续性管理、符合性等一系列内容，要求企业必须构筑高规格的信息安全体系，在实操过程中确保用户信息安全及运营系统的高稳定性。</p>

<p>通过 ISO/IEC27001 认证也再一次证明了码云 Gitee 对信息安全的重视程度以及完备的数据保护策略。代码是企业的核心资产，作为代码托管平台，码云一直竭力保护每一位用户的数据安全。详细内容可见<a href="https://link.zhihu.com/?target=https%3A//blog.gitee.com/2019/04/24/gitee_safe_02/" target="_blank">码云如何保护你的数据 &mdash;&mdash; 系统和网络篇</a>、<a href="https://link.zhihu.com/?target=https%3A//blog.gitee.com/2019/01/07/gitee_safe_01/" target="_blank">码云如何保护你的数据&mdash;&mdash;内部安全治理篇</a>。</p>

<p><img alt="Gitee 通过 ISO27001 与 ISO9001 认证-码云 Gitee 官方博客" height="258" src="https://pic1.zhimg.com/80/v2-205c6afde67f301f016b6c810a7cf86c_hd.jpg" width="387" /></p>

<p>ISO9001 认证是国际公认的高标准的质量管理体系，通过 ISO9001 认证表明企业在各项管理系统整合上已达到了国际标准，并且能持续稳定地为用户提供预期和满意的合格产品。本次ISO 9001认证的通过，是对码云 Gitee 现有的内部管理体系和产品质量的一种肯定，更表明了 Gitee 以用户为中心，满足用户需求，同时不诱导消费者、能够保持品牌中立。</p>

<p>码云 Gitee 在数据安全与产品质量管理上从未有丝毫松懈，ISO27001 与 ISO9001 两项认证就是对码云安全管理与质量管理工作的肯定。未来我们会继续把安全作为第一要务，同时不断完善产品质量，让 Gitee 成为更安全、更可靠、更好用的代码托管与协作开发平台。</p>
