---
layout: post
title: "码云推荐 | 一个 C/C++ 迷你单元测试框架"
---

<p>本项目是受到&nbsp;<a href="http://www.jera.com/techinfo/jtns/jtn002.html">MinUnit</a>&nbsp;的启发而创建的。MinUnit 是一个极简的 C 语言单元测试框架，仅有三行代码，因此其功能亦比较受限。</p>
